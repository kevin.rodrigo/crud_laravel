<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MainController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//route CRUD
Route::get('/',[MainController:: class, 'index']);
Route::get('/tambah',[MainController:: class, 'tambah']);
Route::post('/add',[MainController:: class, 'add']);
Route::get('/edit/{id}',[MainController:: class, 'edit']);
Route::post('/update',[MainControllerr:: class, 'update']);
Route::get('/hapus/{id}',[MainController:: class, 'hapus']);