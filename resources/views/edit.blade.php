<!DOCTYPE html>
<html>
<head>
    <title>Admin</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
        integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/fd8370ec87.js" crossorigin="anonymous"></script>
</head>
<body>
    @include('partials/navbar')

    <div class="mb-4 pl-4">
        <h4>Edit Data Mahasiswa</h4>
        <br>

    @foreach($mahasiswa as $k)
	<form action="/update" method="post">
		{{ csrf_field() }}
 
		<div class="col-lg-6">
            <div class="form-group">
                <input type="hidden" name="id" value="{{ $k->id_mahasiswa }}"> 
            </div>
            <div class="form-group">
                <label for="">Nama</label>
                <input type="text" name="nama" required class="form-control" value="{{ $k->nama_mahasiswa }}">
            </div>
            
            <div class="form-group">
                <label for="">NIM</label>
                <input type="number" name="nim" required class="form-control" value="{{ $k->nim_mahasiswa }}">
            </div>

            <div class="form-group">
                <label for="">Kelas</label>
                <input type="text" name="text" required class="form-control" value="{{ $k->kelas_mahasiswa }}">
    
            </div>

            <div class="form-group">
                <label for="">Prodi</label>
                <input type="text" name="prodi" required class="form-control" value="{{ $k->prodi_mahasiswa }}">
    
            </div>

            <div class="form-group">
                <label for="">Fakultas</label>
                <input type="text" name="fakultas" required class="form-control" value="{{ $k->fakultas_mahasiswa }}">
    
            </div>
            <div class="row"><div class="form-group pl-3">
                <a href="/" class="btn btn-primary">Batal</a>
                <button type="submit" class="btn btn-primary">Save</button>
            </div></div>
            
        </div>
	</form>
	@endforeach
    </div>
    


</body>
</html>