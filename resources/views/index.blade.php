<!DOCTYPE html>
<html>
<head>
    <title>Admin</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
        integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/fd8370ec87.js" crossorigin="anonymous"></script>
</head>
<body>
    @include('partials/navbar')

	<div class="row mb-4 pl-4">
        <div class="pull-left ">
            <h4>DATA MAHASISWA</h4>
        </div>
        <div class="pull-right pl-3 mb-3">
            <a href="/tambah">
                <button class="btn btn-light">Tambah Data Mahasiswa</button>
            </a>
        </div>

        <table class="table ">
            <thead class="thead-light">
                <tr>
                  <th scope="col">ID</th>
                  <th scope="col">Nama</th>
                  <th scope="col">NIM</th>
                  <th scope="col">Kelas</th>
                  <th scope="col">Prodi</th>
                  <th scope="col">Fakultas</th>
                  <th scope="col">Aksi</th>
                </tr>
              </thead>
            </thead>
            <tbody>
                @foreach($mahasiswa as $k)
                <tr>
                    <td>{{ $k->id_mahasiswa }}</td>
                    <td>{{ $k->nama_mahasiswa }}</td>
                    <td>{{ $k->nim_mahasiswa }}</td>
                    <td>{{ $k->kelas_mahasiswa }}</td>
                    <td>{{ $k->prodi_mahasiswa }}</td>
			        <td>{{ $k->fakultas_mahasiswa }}</td>
			        <td>
				        <a href="/edit/{{ $k->id_mahasiswa }}" class="fa fa-pencil"></a>
				        |
				        <a href="/hapus/{{ $k->id_mahasiswa }}" class="fa fa-trash"></a>
			        </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

</body>
</html>