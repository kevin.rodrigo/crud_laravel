<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class MainController extends Controller
{
	public function index()
	{
		$mahasiswa = DB::table('mahasiswa')->get();
		return view('index',['mahasiswa' => $mahasiswa]);

	}

	public function tambah()
	{
		return view('tambah');
	}

	public function add(Request $request)
	{
		DB::table('mahasiswa')->insert([
			'nama_mahasiswa' => $request->nama,
			'nim_mahasiswa' => $request->nim,
			'kelas_mahasiswa' => $request->kelas,
			'prodi_mahasiswa' => $request->prodi,
			'fakultas_mahasiswa' => $request->fakultas
		]);
		return redirect('/');
	}

	public function edit($id)
	{
		$mahasiswa = DB::table('mahasiswa')->where('id_mahasiswa',$id)->get();
		return view('edit',['mahasiswa' => $mahasiswa]);
	}

	public function update(Request $request)
	{
		DB::table('mahasiswa')->where('id_mahasiswa',$request->id)->update([
			'nama_mahasiswa' => $request->nama,
			'nim_mahasiswa' => $request->nim,
			'kelas_mahasiswa' => $request->kelas,
			'prodi_mahasiswa' => $request->prodi,
			'fakultas_mahasiswa' => $request->fakultas,
		]);
		return redirect('/');
	}

	public function hapus($id)
	{
		DB::table('mahasiswa')->where('id_mahasiswa',$id)->delete();
		return redirect('/');
	}
}